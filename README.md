# Configure GitLab project to deploy with Helm

Manages a GitLab deploy token that can authenticate with GitLab's container registry for a project.

Sets the following CI variables in the project:

|Name|Description|
|-|-|
|`HELM_UPGRADE_VALUES`|Values that can be passed to Helm during deployment.|
|`HELM_DOCKER_CREDS_USER`|Deploy token user.|
|`HELM_DOCKER_CREDS_PASSWORD`|Deploy token password.|
|`HELM_DOCKER_CREDS_REGISTRY`|Domain for the container registry to authenticate with.|

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_deploy_token.deploy-token](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_token) | resource |
| [gitlab_project_variable.deploy-registry](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.deploy-token](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.deploy-username](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.helm-values](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project.project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment_scope"></a> [environment\_scope](#input\_environment\_scope) | Environment scope for which the CI variables should be set. | `string` | `"*"` | no |
| <a name="input_helm_values"></a> [helm\_values](#input\_helm\_values) | Helm values in yaml format. | `string` | `null` | no |
| <a name="input_project_id"></a> [project\_id](#input\_project\_id) | Project ID for the GitLab project that should receive the variables. | `number` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->