variable "project_id" {
  type = number

  description = "Project ID for the GitLab project that should receive the variables."
}

variable "helm_values" {
  type    = string
  default = null

  description = "Helm values in yaml format."
}

data "gitlab_project" "project" {
  id = var.project_id
}

variable "environment_scope" {
  type    = string
  default = "*"

  description = "Environment scope for which the CI variables should be set."
}
