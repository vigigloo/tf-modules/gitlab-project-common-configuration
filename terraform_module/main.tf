resource "gitlab_project_variable" "helm-values" {
  count             = var.helm_values == null ? 0 : 1
  project           = data.gitlab_project.project.id
  key               = "HELM_UPGRADE_VALUES"
  variable_type     = "file"
  value             = var.helm_values
  protected         = false
  environment_scope = var.environment_scope
}

resource "gitlab_deploy_token" "deploy-token" {
  project = data.gitlab_project.project.id
  name    = "CI token"
  scopes  = ["read_registry"]
}

resource "gitlab_project_variable" "deploy-username" {
  project           = data.gitlab_project.project.id
  key               = "HELM_DOCKER_CREDS_USER"
  value             = gitlab_deploy_token.deploy-token.username
  protected         = false
  masked            = true
  environment_scope = var.environment_scope
}

resource "gitlab_project_variable" "deploy-token" {
  project           = data.gitlab_project.project.id
  key               = "HELM_DOCKER_CREDS_PASSWORD"
  value             = gitlab_deploy_token.deploy-token.token
  protected         = false
  masked            = true
  environment_scope = var.environment_scope
}

resource "gitlab_project_variable" "deploy-registry" {
  project           = data.gitlab_project.project.id
  key               = "HELM_DOCKER_CREDS_REGISTRY"
  value             = "registry.gitlab.com"
  protected         = false
  environment_scope = var.environment_scope
}
